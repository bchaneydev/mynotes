import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';

import App from './components/App';

import './scss/style.scss';

ReactDOM.render(
  <React.StrictMode>
    <Router basename='dev/mynotes/'>
      <App />
    </Router>
  </React.StrictMode>,
  document.getElementById('notes-app')
);
