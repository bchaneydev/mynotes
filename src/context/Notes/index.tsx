import React, { createContext, Dispatch, useReducer } from 'react';
import NotesReducer from '../../reducers/Notes';
import { Note, ActionKind } from '../../types';

const initialState = {
  notes: [
    {
      title: 'Shopping list',
      description:
        'Apples, pears, carrots, scotch fillet steak.',
      noteid: 1
    },
    {
      title: 'Passage from 1984',
      description:
        '"Who controls the past," ran the Party slogan, "controls the future: who controls the present controls the past. " And yet the past, though of its nature alterable, never had been altered. Whatever was true now was true from everlasting to everlasting.',
      noteid: 2
    },
    {
      title: 'My todo list',
      description:
        'Start running 3x week and eating healthier.',
      noteid: 3
    }
  ],
  addNote: (note: Note) => {},
  editNote: (note: Note) => {},
  deleteNote: (noteid: number) => {}
};

export const NotesContext = createContext(initialState);

export const NotesContextProvider = ({ children }: { children?: React.ReactNode }) => {
  const [state, dispatch] = useReducer(NotesReducer, initialState);

  function addNote(note: Note) {
    dispatch({
      type: ActionKind.Add,
      payload: note
    });
  }

  function editNote(note: Note) {
    dispatch({
      type: ActionKind.Edit,
      payload: note
    });
  }

  function deleteNote(noteid: number) {
    dispatch({
      type: ActionKind.Delete,
      payload: noteid
    });
  }

  return (
    <NotesContext.Provider
      value={{
        notes: state.notes,
        addNote,
        editNote,
        deleteNote
      }}
    >
      {children}
    </NotesContext.Provider>
  );
};
