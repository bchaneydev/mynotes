import React, { useState, useEffect } from 'react';
import Button from '../Form/Button';
import useBrowserWidth from '../../hooks/useBrowserWidth';

import './style.scss';

const Welcome = () => {
  const [arrowClass, setArrowClass] = useState('bi-arrow-left-short');

  const browserWidth = useBrowserWidth(300);

  useEffect(() => {
    browserWidth < 576 ? setArrowClass('bi-arrow-up-short') : setArrowClass('bi-arrow-left-short');
  }, [browserWidth, setArrowClass]);

  return (
    <div className="row h-100">
      <div className="welcome col text-center">
        <h2>
          Welcome to <span>my</span>notes
        </h2>
        <p>
          <i className={`bi ${arrowClass}`}></i> Select a note to view and
          edit.
        </p>
        <p>Create a new note by clicking the button below...</p>
        <Button title="New note">
          <i className="bi bi-pencil"></i>
        </Button>
      </div>
    </div>
  );
};

export default Welcome;
