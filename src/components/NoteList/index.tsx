import React, { useContext } from 'react';
import { NotesContext } from '../../context/Notes';
import NoteListItem from './NoteListItem';

import './style.scss';

const NoteList = () => {
  const { notes } = useContext(NotesContext);

  const listNotes = notes
    .map((note) => <NoteListItem key={note.noteid} note={note} />)
    .reverse();

  return (
    <div className="list-col">
      <ul>{listNotes}</ul>
    </div>
  );
};

export default NoteList;
