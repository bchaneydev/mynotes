import React, { useContext } from 'react';
import { useHistory } from 'react-router-dom';
import { NotesContext } from '../../../context/Notes';

import './style.scss';

type Props = {
  title: string;
  children?: React.ReactNode;
};

const Button = ({ title, children }: Props) => {
  const history = useHistory();
  const { notes, addNote } = useContext(NotesContext);

  const addNoteHandler = () => {
    let newId = 1;

    if (notes.length > 0) {
      newId = [...notes].sort((a, b) => b.noteid - a.noteid)[0].noteid + 1;
    }

    addNote({
      title: 'New note...',
      description: '',
      noteid: newId
    });

    history.push(`/note/${newId}`);
  };

  return (
    <button
      type="button"
      onClick={addNoteHandler}
      title={title}
      className="btn btn-primary mt-3"
    >
      {children}
    </button>
  );
};

export default Button;
