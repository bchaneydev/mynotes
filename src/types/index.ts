export type Note = {
  title: string;
  description: string;
  noteid: number;
}

export type NotesContextProps = {
  notes: {
    title: string;
    description: string;
    noteid: number;
  }[];
  addNote: (note: Note) => void;
  editNote: (note: Note) => void;
  deleteNote: (noteid: number) => void;
}

export enum ActionKind {
  Add = 'ADD_NOTE',
  Edit = 'EDIT_NOTE',
  Delete = 'DELETE_NOTE',
}