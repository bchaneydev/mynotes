import { Note, ActionKind } from '../../types';

type State = {
  notes: {
    title: string;
    description: string;
    noteid: number;
  }[];
};

type Action =
  | { type: 'ADD_NOTE' | 'EDIT_NOTE'; payload: Note }
  | { type: 'DELETE_NOTE'; payload: number };

const NotesReducer = (state: State, action: Action) => {
  if (action.type === ActionKind.Add) {
    console.log('add');

    return {
      notes: [...state.notes, action.payload]
    };
  }

  if (action.type === ActionKind.Edit) {
    console.log('edit');
    const unchangedNotes = state.notes.filter(
      (note) => note.noteid !== action.payload.noteid
    );

    const notes = [action.payload, ...unchangedNotes];
    return {
      notes: notes.sort((a, b) => {
        return a.noteid - b.noteid;
      })
    };
  }

  if (action.type === ActionKind.Delete) {
    console.log('delete', action.payload);
    const filteredNotes = state.notes.filter(
      (note) => note.noteid !== action.payload
    );
    return {
      notes: [...filteredNotes]
    };
  }

  return state;
};

export default NotesReducer;
