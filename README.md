# mynotes web app

---

Live preview: [https://www.brettchaney.com/dev/mynotes](https://www.brettchaney.com/dev/mynotes)

A fully responsive web app that can be used to take notes for everyday use.

### Tools

The following is a list of tools used to develop the mynotes app:

- HTML
- CSS/SASS
- React.js
- (React) Context API
- Hooks w/custom hooks
- JavaScript incl. ES6
- TypeScript
